import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    player1: '',
    player2: '',
    picked: 0
  },
  mutations: {
    updatePlayer1 (state, player1) {
      state.player1 = player1
    },
    updatePlayer2 (state, player2) {
      state.player2 = player2
    },
    updatePicked (state, picked) {
      state.picked = picked
    }
  },
  getters: {
    player1: state => state.player1,
    player2: state => state.player2,
    picked: state => state.picked
  },
  actions: {
  },
  modules: {
  }
})
