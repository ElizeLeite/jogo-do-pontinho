# Jogo do Pontinho
Projeto desenvolvido para simular o jogo Timbiriche, conhecido como Jogo dos Pontinhos, onde será jogado por duas pessoas em um mapa de pontos e o jogador que fizer mais quadrados interligando esses pontos será o vencedor.

## Começando
Para execução do projeto é necessário instalar algumas dependências:
- Vue CLI
- Vue SweetAlert2
```
npm install
```

## Desenvolvimento
Para iniciar o desenvolvimento ou rodar o projeto, é necessário clonar o projeto do GitLab em um diretório de sua preferência:

```
cd "diretorio-de-sua-preferencia"
git clone https://gitlab.com/ElizeLeite/jogo-do-pontinho.git
```

## Comando para execução
Para ser executado é necessário estar na pasta do projeto
```
cd "jogo-do-pontinho"
npm run serve
```

### Para buildar
```
cd "jogo-do-pontinho"
npm run build
```

### Features
Iniciando como um mapa de pontos, os jogadores revezam turnos e precisam marcar linhas horizontais e verticais entre tais pontos. O jogador que completar a quarta aresta de um quadrado 1×1 recebe um ponto e ganha mais um turno, além de marcar a inicial de seu nome no quadrado. O jogo é finalizado quando não existir mais linhas à serem criadas, e vence quem no final tiver o maior número de quadrados 1x1 criados.

### Links
https://pt.wikipedia.org/wiki/Timbiriche_(jogo)

### Licença
Não se aplica
